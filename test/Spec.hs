import Lib
import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)

main :: IO ()
main = hspec $ do
  describe "Lib" $ do
    it "myfoldr work like foldr, but use `Fixed point combinator`" $ do
      let xs = [1,2,3,4,5]
      myfoldr (:) [] xs        `shouldBe` [1,2,3,4,5]
      myfoldr (+) 0 xs         `shouldBe` 15
      myfoldr (*) 1 xs         `shouldBe` 120
      myfoldr max (head xs) xs `shouldBe` 5

    it "equilateral triangles have equal sides" $ do
      triangle(2, 2, 2)    `shouldBe` Equilateral
      triangle(10, 10, 10) `shouldBe` Equilateral

    it "isosceles triangles have 2 equal sides" $ do
      triangle(3, 4, 4)    `shouldBe` Isosceles
      triangle(4, 3, 4)    `shouldBe` Isosceles
      triangle(4, 4, 3)    `shouldBe` Isosceles
      triangle(10, 10, 2)  `shouldBe` Isosceles

    it "scalene triangles have no equal sides" $ do
      triangle(3, 4, 5)    `shouldBe` Scalene
      triangle(10, 11, 12) `shouldBe` Scalene
      triangle(5, 4, 2)    `shouldBe` Scalene

    -- it "illegal triangles throw an exception" $ do
    --   evaluate (triangle(0, 0, 0) ) `shouldThrow` TriangleError
    --   evaluate (triangle(3, 4, -5)) `shouldThrow` TriangleError
    --   evaluate (triangle(1, 1, 3) ) `shouldThrow` TriangleError
    --   evaluate (triangle(2, 4, 2) ) `shouldThrow` TriangleError
