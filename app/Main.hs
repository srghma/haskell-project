module Main where

import Lib
import Data.List

import Prelude hiding (id, (>>), pred, Maybe(..), (*>), Kleisli(..))

class Category cat where
  id :: cat a a
  (>>) :: cat a b -> cat b c -> cat a c

class Kleisli m where
  idK :: a -> m a
  (*>) :: (a -> m b) -> (b -> m c) -> (a -> m c)

(+>) :: Kleisli m => (a -> m b) -> (b -> c) -> (a -> m c)
f +> g = f *> (g >> idK)

-- Экземпляр для функций

instance Category (->) where
  id = \x -> x
  f >> g = \x -> g (f x)

data Maybe a = Nothing | Just a
  deriving (Show, Eq, Ord)

instance Kleisli Maybe where
  idK =
    f *> g = \a -> case f a of
                      Nothing -> Nothing
                      Just b -> g b

func = \a -> (a, a + 2)

main :: IO ()
main = do print . func $ 1
