module Lib
    ( triangle
    , TriangleType(..)
    , myfoldr
    ) where

import Data.List
import Data.Function

data TriangleType = Equilateral | Isosceles | Scalene
  deriving (Show, Eq)

triangle :: (Int, Int, Int)-> TriangleType
triangle (a, b, c) = let arr = sort [a, b, c]
                         types = [Equilateral, Isosceles, Scalene]
                      in types !! ((length . nub $ arr) - 1)

myfoldr :: (a -> b -> b) -> b -> [a] -> b
myfoldr func z = fix f
  where f t = \xs -> case xs of
                          [] -> z
                          (x':xs') -> func x' (t xs')
